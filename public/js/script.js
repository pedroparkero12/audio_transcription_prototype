

angular.module("myApp",['angular-loading-bar'])

.controller("appController", ["$scope","$timeout","$http","$window",
    function ($scope,$timeout,$http,$window) {
      try {
        var recognition = new webkitSpeechRecognition();
      }
      catch(e) {
        console.error(e);
        $('.no-browser-support').show();
        $('.app').hide();
      }
      
      var instructions = $('#recording-instructions');
      var noteContent = '';
      
      //recognitions settings
      
      recognition.continuous = true;
      
      recognition.onresult = function(event) {
        var current = event.resultIndex;
        var transcript = event.results[current][0].transcript;
        var mobileRepeatBug = (current == 1 && transcript == event.results[0][0].transcript);
        if(!mobileRepeatBug) {
          noteContent += transcript;
        }
      };
      
      recognition.onstart = function() { 
        instructions.text('Voice recognition activated. Try speaking into the microphone.');
        $('#start-record-btn').hide();
        $('#pause-record-btn').show();
        noteContent = '';
      }
      
      recognition.onspeechend = function() {
        instructions.text('You were quiet for a while so voice recognition turned itself off.');
        $('#start-record-btn').show();
        $('#pause-record-btn').hide();
      }
      
      recognition.onerror = function(event) {
        if(event.error == 'no-speech') {
          instructions.text('No speech was detected. Try again.');
          $('#start-record-btn').show(); 
          $('#pause-record-btn').hide();
        };
      }
      
      //button events
      $scope.start = function(){
        if (noteContent.length) {
          noteContent += '';
        }
        recognition.start();
      }

      $scope.end = function(){
        recognition.stop();
        instructions.text('Voice recognition paused.');
        $('#start-record-btn').show();
        $('#pause-record-btn').hide();
        
        $timeout(function(){
          $scope.analysis();
        },1000); 
      }
      
      //analysis 
      $scope.analysisResult = {};
      $scope.analysis = function(){
        // $.post('/analysis',{string:noteContent},function(data){
        //   console.log(data);
        //   $scope.analysisResult = data;
        // });
        $http.post('/analysis',{string:noteContent}).then(function(data){
          $scope.analysisResult = data.data;
        });
      }
      
      //show synset info
      $scope.showSynset = function(id){
        $window.open('https://babelnet.org/synset?word='+id, '_blank');
      }

    }
]);

