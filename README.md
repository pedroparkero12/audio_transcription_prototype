Simple audio transcription system prototype using:
*webkit speech recognition
*google cloud analyzing entities
*babelnet

to install run "npm run install"
to run the server "npm run server"
to run the system open the chrome browser and open "localhost:3000"