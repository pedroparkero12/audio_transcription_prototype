const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https:*//firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const axios = require('axios')

//apikey for google cloud
const googleKey = 'AIzaSyBIa3yerhLlS0EkCsIJNIWAIF8axtcT_KQ'
const babelKey = '090e235c-59ab-49c2-aa59-4bbfe934c93e'

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// app.get('/analysis', (req, res) => {
//     res.send('Hello from express app');
// })

//perform analysis
app.post('/analysis', (req, res) => {
    let string = req.body.string
    let config = {
      headers: {'Content-Type': 'application/json; charset=utf-8'}
    };
    axios.post(`https://language.googleapis.com/v1/documents:analyzeEntities?key=${googleKey}`, 
      {
        'encodingType': 'UTF8',
        'document': {
          'type': 'PLAIN_TEXT',
          'content': string
        }
      }
    , config)
      
      .then((data1)=>{
        let resp = data1.data;
        let searchLang = resp.language;
        let entities = resp.entities;
        let arrayResult = {string:string,result:[]}
        if(entities.length > 0) {
          let y = 1;
          for(let x of entities){
            queryEntities(x.name,searchLang).then((data2)=>{
              arrayResult.result.push({name: x.name, synset: data2.data})
              if(y==entities.length) res.send(arrayResult)
              y++
            })
          }
        }
        else{
          res.send(arrayResult)
        }
        
      })
      .catch(err => res.status(500).send('Invalid key text'))
})

//query recognized entities
function queryEntities(lemma,searchLang){
    return axios.get(`https://babelnet.io/v5/getSynsetIds?lemma=${lemma}&searchLang=${searchLang}&key=${babelKey}`)
}

exports.app = functions.https.onRequest(app)




